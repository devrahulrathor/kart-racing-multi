﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class leftBtn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public static bool left_pressed = false;
    // Start is called before the first frame update
    //bool ispressed = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        left_pressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        left_pressed = false;
    }
}
