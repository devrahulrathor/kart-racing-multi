﻿
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Drive ds;
    float lastTimeMoving = 0.0f;
    Vector3 lastPosition;
    Quaternion lastRotation;

    CheckpointManager cpm;
    float finishSteer;

    void ResetLayer() {

        ds.rb.gameObject.layer = 0;
        this.GetComponent<Ghost>().enabled = false;
    }

    void Start() {

        ds = this.GetComponent<Drive>();
        this.GetComponent<Ghost>().enabled = false;
        lastPosition = ds.rb.gameObject.transform.position;
        lastRotation = ds.rb.gameObject.transform.rotation;
        finishSteer = Random.Range(-1.0f, 1.0f);
    }

    void Update() {

        if (cpm == null) {

            cpm = ds.rb.GetComponent<CheckpointManager>();
        }

        if (cpm.lap == RaceMonitor.totalLaps + 1) {

            ds.highAccel.Stop();
            ds.Go(0.0f, finishSteer, 0.0f);
        }

        float a = Input.GetAxis("Vertical");
        float s = Input.GetAxis("Horizontal");
        float b = Input.GetAxis("Jump");
        
        var tilt= Input.acceleration;
        //Debug.Log("Before_ "+tilt.x + "X ." + tilt.y + "Y ." + tilt.z + "Z");
        tilt = Quaternion.Euler(90, 0, 0)*tilt;
        //Debug.Log(tilt.x + "X ." + tilt.y + "Y ." + tilt.z + "Z");
        s = tilt.x * 0.9f;
        if (chnageConltoll.rotation_change)
        {

        }
        else
        {
            if (leftBtn.left_pressed && RightBtn.Right_pressed)
            {
                s = 0;
            }
            else if (leftBtn.left_pressed && !RightBtn.Right_pressed)
            {
                s = 1;
            }
            else if (!leftBtn.left_pressed && RightBtn.Right_pressed)
            {
                s = -1;
            }
            else
            {
                s = 0;
            }
        }
        if (ds.rb.velocity.magnitude > 1.0f || !RaceMonitor.racing) {

            lastTimeMoving = Time.time;
        }

        RaycastHit hit;
        if (Physics.Raycast(ds.rb.gameObject.transform.position, -Vector3.up, out hit, 10)) {

            if (hit.collider.gameObject.tag == "road") {

                lastPosition = ds.rb.gameObject.transform.position;
                lastRotation = ds.rb.gameObject.transform.rotation;
            }
        }

        if (Time.time > lastTimeMoving + 4 || ds.rb.gameObject.transform.position.y < -5.0f) {
            ds.rb.gameObject.transform.position = cpm.lastCP.transform.position + Vector3.up * 2;
            ds.rb.gameObject.transform.rotation = cpm.lastCP.transform.rotation;
            ds.rb.gameObject.layer = 8;
            //this.GetComponent<Ghost>().enabled = true;
            Invoke("ResetLayer", 3);
        }
        //int i = 1;
        foreach (var camm in Camera.allCameras)
        {
            if (camm.name == "RearCamera")
                camm.enabled = false;
            if(camm.name== "TrackCam")
            {
                camm.enabled = false;
            }
            //Debug.Log("Camera"+i+" Enable" +camm.enabled);
            //Debug.Log("Camera" + i + " Name" + camm.name);
            //Debug.Log("Camera" + i + " possiotion" + camm.transform.position);
            //i++;
        }
        if(accelbtn.accel_pressed && breakbtn.Break_pressed)
        {
            a = 0;
        }
        else if(accelbtn.accel_pressed && !breakbtn.Break_pressed)
        {
            a = 1;
        }
        else if (!accelbtn.accel_pressed && breakbtn.Break_pressed)
        {
            a = -1;
        }
        else
        {
            a = 0;
        }
        //Debug.Log("Cmera Nameeee_" + Camera.current.name);
        
        //Debug.Log("Racing Gameeee  --" + RaceMonitor.racing);
        //Debug.Log("Acccccc --" + a);
        //Debug.Log("SSSSSScccc --" + s);
        //Debug.Log("BBBccccc --" + b);
        //a = 1f;
        if (!RaceMonitor.racing) a = 0.0f;
        ds.Go(a, s, b);

        ds.CheckForSkid();
        ds.CalculateEngineSound();
    }
}
